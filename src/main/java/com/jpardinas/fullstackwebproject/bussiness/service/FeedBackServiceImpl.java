package com.jpardinas.fullstackwebproject.bussiness.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jpardinas.fullstackwebproject.bussiness.FeedBackService;
import com.jpardinas.fullstackwebproject.persistence.dao.FeedBackDAO;
import com.jpardinas.fullstackwebproject.persistence.transform.FeedBackEntityTransform;
import com.jpardinas.fullstackwebproject.web.model.FeedBackBean;
import com.jpardinas.fullstackwebproject.web.model.FeedBackForm;
import com.jpardinas.fullstackwebproject.web.transform.FeedBackBeanTransform;

@Service("feedBackService")
public class FeedBackServiceImpl implements FeedBackService {
	
	@Autowired
	private FeedBackDAO feedBackDao;
	
	@Autowired
	@Qualifier("feedBackEntityTransform")
	private FeedBackEntityTransform feedBackEntityTransform;
	
	@Autowired
	@Qualifier("feedBackBeanTransform")
	private FeedBackBeanTransform feedBackBeanTransform;
	
	
	@Override
	@Transactional
	public List<FeedBackBean> getFeedBackBean() {
		return feedBackBeanTransform.transformFeedBackBussiness2FeedBackBean(
				feedBackEntityTransform.transformFeedBackEntity2FeedBackBussiness(
						feedBackDao.getFeedBacks()));
	}



	@Override
	@Transactional
	public FeedBackBean getFeedBackBeanById(final FeedBackForm feedBackForm) {
		return feedBackBeanTransform.transformFeedBackBussiness2FeedBackBean(
				feedBackEntityTransform.transformFeedBackEntity2FeedBackBussiness(
						feedBackDao.getFeedBackById(feedBackForm.getId())));
	}

	

}
