package com.jpardinas.fullstackwebproject.bussiness;

import java.util.List;

import com.jpardinas.fullstackwebproject.web.model.FeedBackBean;
import com.jpardinas.fullstackwebproject.web.model.FeedBackForm;

public interface FeedBackService {
	
	List<FeedBackBean> getFeedBackBean();
	
	FeedBackBean getFeedBackBeanById(final FeedBackForm feedBackForm);

}
