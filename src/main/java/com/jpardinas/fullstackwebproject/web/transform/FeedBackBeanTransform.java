package com.jpardinas.fullstackwebproject.web.transform;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.jpardinas.fullstackwebproject.bussiness.model.FeedBackBussiness;
import com.jpardinas.fullstackwebproject.web.model.FeedBackBean;

@Component("feedBackBeanTransform")
public class FeedBackBeanTransform {
	
	public List<FeedBackBean> transformFeedBackBussiness2FeedBackBean (final List<FeedBackBussiness> listFeedBackBussiness) {
		
		final List<FeedBackBean> listFeedBackBean = new ArrayList<FeedBackBean>();
		for (final FeedBackBussiness feedBackBussiness: listFeedBackBussiness) {
			listFeedBackBean.add(transformFeedBackBussiness2FeedBackBean(feedBackBussiness));
		}
		return listFeedBackBean;
	}
	
	
	public FeedBackBean transformFeedBackBussiness2FeedBackBean (final FeedBackBussiness feedBackBussiness) {
		
		final FeedBackBean feedBackBean = new FeedBackBean();
		feedBackBean.setId(feedBackBussiness.getId());
		feedBackBean.setEmail(feedBackBussiness.getEmail());
		feedBackBean.setName(feedBackBussiness.getName());
		feedBackBean.setMessage(feedBackBussiness.getMessage());
		feedBackBean.setCode(feedBackBussiness.getCode());
		feedBackBean.setRegarding(feedBackBussiness.getRegarding());
		feedBackBean.setRating(feedBackBussiness.getRating());
		
		return feedBackBean;
	}
	

}
