package com.jpardinas.fullstackwebproject.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
	
	@RequestMapping(value = { "/", "index" })
    public ModelAndView getIndex() {
        return new ModelAndView("public/index");
    }
	
	@RequestMapping(value = "/private/index")
    public ModelAndView getIndexPrivate() {
        return new ModelAndView("private/index");
    }
	

}
