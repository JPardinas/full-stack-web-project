package com.jpardinas.fullstackwebproject.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @GetMapping("/login")
    public ModelAndView loginForm() {
        return new ModelAndView("public/login");
    }

    @GetMapping("/succesLogin")
    public ModelAndView loginSucess() {
        return new ModelAndView("redirect:private/administracion");
    }
}
