package com.jpardinas.fullstackwebproject.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.jpardinas.fullstackwebproject.bussiness.FeedBackService;

@Controller
@RequestMapping("/feedback")
public class FeedBackController {

    @Autowired
    private FeedBackService feedBackService;

    @GetMapping
    public ModelAndView devolverListaAutores() {
        return new ModelAndView("public/feedbackViews/feedback").addObject("list", feedBackService.getFeedBackBean());
    }
}
