package com.jpardinas.fullstackwebproject.web.controller;

import org.codehaus.jettison.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdministracionController {

    

    @GetMapping("/administracion")
    public ModelAndView accesoAdministracion() throws JSONException {
    	return new ModelAndView("private/administracion");
    }

    
}
