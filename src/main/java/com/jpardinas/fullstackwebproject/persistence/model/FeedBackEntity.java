package com.jpardinas.fullstackwebproject.persistence.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.validation.constraints.NotNull;

@Entity
@Table(name = "feedback")
public class FeedBackEntity implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	@Column(name = "id")
	private Long id;
	
	@NotNull
	@Column(name = "email", columnDefinition = "VARCHAR(120)")
	private String email;
	
	@Column(name = "name", columnDefinition = "VARCHAR(30)")
	private String name;
	
	@Column(name = "regarding", columnDefinition = "VARCHAR(500)")
	private String regarding;
	
	@Column(name = "code", columnDefinition = "VARCHAR(10)")
	private String code;
	
	@Column(name = "rating", columnDefinition = "INTEGER(11)")
	private int rating;
	
	@Column(name = "message", columnDefinition = "VARCHAR(10000)")
	private String message;
	

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRegarding() {
		return regarding;
	}
	public void setRegarding(String regarding) {
		this.regarding = regarding;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "FeedBackEntity [name=" + name + ", email=" + email + ", regarding=" + regarding + ", code=" + code
				+ ", rating=" + rating + ", message=" + message +"]";
	}
	
	
	

}
