package com.jpardinas.fullstackwebproject.persistence.dao;

import java.util.List;

import com.jpardinas.fullstackwebproject.persistence.model.FeedBackEntity;

public interface FeedBackDAO {
	
//	void addFeedBack(FeedBackEntity feedBackEntity);
	
	List<FeedBackEntity> getFeedBacks();
	
	FeedBackEntity getFeedBackById(final Long id);

}
