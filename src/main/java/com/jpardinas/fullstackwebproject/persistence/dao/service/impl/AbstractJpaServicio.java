package com.jpardinas.fullstackwebproject.persistence.dao.service.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jpardinas.fullstackwebproject.persistence.dao.service.JpaServicio;


public abstract class AbstractJpaServicio implements JpaServicio {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public <T> T persist(final T object) {
        sessionFactory.getCurrentSession().persist(object);
        return object;
    }

    @Override
    public <T> T delete(final T object) {
        sessionFactory.getCurrentSession().delete(object);
        return object;
    }

    @Override
    public <T> T update(final T object) {
        sessionFactory.getCurrentSession().update(object);
        return object;
    }
}
