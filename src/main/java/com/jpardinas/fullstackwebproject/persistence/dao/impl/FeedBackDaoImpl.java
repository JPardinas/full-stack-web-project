package com.jpardinas.fullstackwebproject.persistence.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.jpardinas.fullstackwebproject.persistence.dao.FeedBackDAO;
import com.jpardinas.fullstackwebproject.persistence.model.FeedBackEntity;

@Repository
@Component("feedBackDao")
public class FeedBackDaoImpl implements FeedBackDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	

	@Override
	public List<FeedBackEntity> getFeedBacks() {
		
		final Session session = sessionFactory.getCurrentSession();
        final CriteriaBuilder criBuilder = session.getCriteriaBuilder();
        final CriteriaQuery<FeedBackEntity> criQuery = criBuilder.createQuery(FeedBackEntity.class);
        final Root<FeedBackEntity> root = criQuery.from(FeedBackEntity.class);
        criQuery.select(root);

        final TypedQuery<FeedBackEntity> query = session.createQuery(criQuery);

        return query.getResultList();
	}



	@Override
	public FeedBackEntity getFeedBackById(final Long id) {
		final Session session = sessionFactory.getCurrentSession();
        final CriteriaBuilder criBuilder = session.getCriteriaBuilder();
        final CriteriaQuery<FeedBackEntity> criQuery = criBuilder.createQuery(FeedBackEntity.class);
        final Root<FeedBackEntity> root = criQuery.from(FeedBackEntity.class);
        criQuery.where(criBuilder.equal(root.get("id"), id));

        final TypedQuery<FeedBackEntity> query = session.createQuery(criQuery);

        return query.getSingleResult();
	}

	
}
