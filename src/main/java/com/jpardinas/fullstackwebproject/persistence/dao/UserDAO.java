package com.jpardinas.fullstackwebproject.persistence.dao;

import com.jpardinas.fullstackwebproject.persistence.model.UserEntity;

public interface UserDAO {
    UserEntity findByUsername(String username);
}
