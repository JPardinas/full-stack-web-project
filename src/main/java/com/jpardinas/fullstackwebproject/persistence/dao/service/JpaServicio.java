package com.jpardinas.fullstackwebproject.persistence.dao.service;

public interface JpaServicio {

    <T> T persist(T object);

    <T> T delete(T object);

    <T> T update(T object);
}
