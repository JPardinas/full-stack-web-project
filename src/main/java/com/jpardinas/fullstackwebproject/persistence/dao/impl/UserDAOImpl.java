package com.jpardinas.fullstackwebproject.persistence.dao.impl;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jpardinas.fullstackwebproject.persistence.dao.UserDAO;
import com.jpardinas.fullstackwebproject.persistence.model.UserEntity;



@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public UserEntity findByUsername(final String username) {

        final Session session = sessionFactory.openSession();
        final CriteriaBuilder criBuilder = session.getCriteriaBuilder();
        final CriteriaQuery<UserEntity> criQuery = criBuilder.createQuery(UserEntity.class);
        final Root<UserEntity> librosEntityRoot = criQuery.from(UserEntity.class);
        criQuery.where(criBuilder.equal(librosEntityRoot.get("username"), username));

        final TypedQuery<UserEntity> query = session.createQuery(criQuery);

        return query.getSingleResult();
    }

}
