package com.jpardinas.fullstackwebproject.persistence.transform;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.jpardinas.fullstackwebproject.bussiness.model.FeedBackBussiness;
import com.jpardinas.fullstackwebproject.persistence.model.FeedBackEntity;

@Component("feedBackEntityTransform")
public class FeedBackEntityTransform {
	
	
	public List<FeedBackBussiness> transformFeedBackEntity2FeedBackBussiness (final List<FeedBackEntity> listFeedBackEntity) {
		
		final List<FeedBackBussiness> listFeedBackBussiness = new ArrayList<FeedBackBussiness>();

		for (final FeedBackEntity feedBackEntity: listFeedBackEntity) {
			listFeedBackBussiness.add(transformFeedBackEntity2FeedBackBussiness(feedBackEntity));
		}
		
		return listFeedBackBussiness;
	}
	
	
	public FeedBackBussiness transformFeedBackEntity2FeedBackBussiness (final FeedBackEntity feedBackEntity) {
		
		final FeedBackBussiness feedBackBussiness = new FeedBackBussiness();
		feedBackBussiness.setId(feedBackEntity.getId());
		feedBackBussiness.setEmail(feedBackEntity.getEmail());
		feedBackBussiness.setName(feedBackEntity.getName());
		feedBackBussiness.setMessage(feedBackEntity.getMessage());
		feedBackBussiness.setCode(feedBackEntity.getCode());
		feedBackBussiness.setRegarding(feedBackEntity.getRegarding());
		feedBackBussiness.setRating(feedBackEntity.getRating());
		
		return feedBackBussiness;
	}

}
