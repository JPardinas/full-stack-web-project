<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">		
		<link rel="stylesheet" href="webjars/mdbootstrap-bootstrap-material-design/4.7.4/css/bootstrap.min.css">
    	<link rel="stylesheet" href="webjars/mdbootstrap-bootstrap-material-design/4.7.4/css/mdb.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">	   
		<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<title>Full Stack Web Project</title>
    </head>  
	<body>
		<header>
			<div class="container-fluid mt-5 ">
				<div class="jumbotron text-center blue-gradient">
		            <h1 class="text-white">Full Stack Web Project</h1>
		            <p class="my-4 font-weight-bold text-white-50">Test</p>
				</div>
			</div>
		</header>
		
		<main>
			<div class="container">
				<div class="row">
					<div class="card mb-5">
					 	<div class="text-white text-center align-items-center unique-color py-5 px-4">
					    	<div>
					    		<h3 class="card-title pt-2">FeedBacks</h3>
					      		<p>Go to feedbacks site</p>
					      		<a class="btn btn-mdb-color" href="feedback">Go&nbsp;&nbsp;<i class="fas fa-arrow-right"></i> </a>
					    	</div>
						</div>
					</div>			
				</div>
			</div>
		</main>
		
		<script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/jquery-3.3.1.min.js"></script>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/popper.min.js"></script>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/mdb.min.js"></script>
	</body>
</html>