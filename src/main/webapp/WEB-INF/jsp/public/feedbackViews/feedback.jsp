<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:jsp="http://java.sun.com/JSP/Page"
      xmlns:c="http://java.sun.com/jsp/jstl/core">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="webjars/mdbootstrap-bootstrap-material-design/4.7.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="webjars/mdbootstrap-bootstrap-material-design/4.7.4/css/mdb.min.css">
        <link rel="stylesheet" href="webjars/mdbootstrap-bootstrap-material-design/4.7.4/css/addons/datatables.min.css">
  		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
        <%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>          
        <title>Feed Back List</title>
	</head>
	<body>
	    <header>
	    	<div class="container-fluid mt-5 ">
	        	<div class="jumbotron text-center purple-gradient">
		        	<h1>Feed Back List</h1>
		        	<a href="index" class="btn transparent"><i class="fas fa-arrow-left"></i>&nbsp;&nbsp;Return</a>	
		        </div>
	        </div>
	    </header>
	    <main>
		    <div class="container">
		    	<div class="table-responsive">
			    	<table id="tablaAutores" class="table table-striped table-hover table-bordered" cellspacing="0">
			            <thead class="thead-light">
			              <tr>
			                <th>Id</th>
			                <th>E-mail</th>
			                <th>Name</th>
			                <th>Regarding</th>
			                <th>Rating</th>
			              </tr>
			            </thead>
			            <tbody>
			                <c:forEach items="${list}" var="feedback">
			                      <tr>
			                        <td><c:out value="${feedback.getId()}"/></td>
			                        <td><c:out value="${feedback.getEmail()}"/></td>
			                        <td><c:out value="${feedback.getName()}"/></td>   
			                        <td><c:out value="${feedback.getRegarding()}"/></td>
			                        <td><c:out value="${feedback.getRating()}"/></td>
			                      </tr>
			                  </c:forEach>
			            </tbody>
		        	</table>
		        </div>
		    </div>
	    </main>
	    <footer>
	    </footer>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/jquery-3.3.1.min.js"></script>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/popper.min.js"></script>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/mdb.min.js"></script>1
	    <script type="text/javascript" src="webjars/mdbootstrap-bootstrap-material-design/4.7.4/js/addons/datatables.min.js"></script>
	    <script type="text/javascript" src="resources/js/iniciarDatatables.js"></script>
	</body>
</html>